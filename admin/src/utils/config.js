let config = {
    项目名称: "食谱",
    接口网址: "http://localhost:8040/接口/",
    验证码网址: "http://localhost:8040/验证码图片/",
    默认地址: "/食谱管理",
    版权所有: "云社区"
}

if (process.env.NODE_ENV != "development") {
    Object.assign(config, {
        接口网址: document.location.origin + "/接口/",
        验证码网址: document.location.origin + "/验证码图片/",
    })
}

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

export default config