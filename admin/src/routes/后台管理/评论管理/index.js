import React, { Component } from 'react'
import { Table, Button, Input, Popconfirm, message } from 'antd'
import request from '../../../utils/request'
import utils from '../../../utils'

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            页: 1,
            页长: 10,
            总数: 0,
            评论列表: [],
            已选Id: [],
            正在加载: false,
            搜索: ""
        }
    }
    componentDidMount = () => {
        this.获取评论列表()
    }
    获取评论列表 = () => {
        this.setState({ 正在加载: true })

        const { 页, 页长, 搜索 } = this.state
        request("评论/管理列表", {
            页, 页长, 搜索
        }, (评论列表, 总数) => {
            this.setState({ 评论列表, 总数, 正在加载: false })
        })
    }

    render() {
        const { 页, 页长, 总数, 评论列表, 选择, 正在加载, 已选Id } = this.state
        const 列 = [{
            title: "ID",
            dataIndex: "Id",
        }, {
            title: "食谱",
            dataIndex: "F食谱.F名称",
        }, {
            title: "联系方式",
            dataIndex: "F联系方式",
        }, {
            title: "评论内容",
            dataIndex: "F评论内容",
        }, {
            title: "创建时间",
            dataIndex: "F创建时间",
            render: (text, record) => {
                return utils.格式化时间(text)
            }
        }]
        return (
            <div>
                <div style={{ marginBottom: 16, display: 'flex', justifyContent: 'space-between' }}>
                    <Input.Search style={{ width: 256 }} enterButton placeholder="输入评论内容" onSearch={搜索 => {
                        this.setState({ 搜索, 页: 1 }, () => {
                            this.获取评论列表()
                        })
                    }} />

                    {已选Id.length > 0 && (<Popconfirm title="确定要删除吗?" onConfirm={() => {
                        request("评论/删除", {
                            Ids: JSON.stringify(已选Id)
                        }, () => {
                            this.获取评论列表()
                        }, (code, msg) => {
                            message(msg)
                        })
                    }}>
                        <Button type="danger">删除</Button>
                    </Popconfirm>)}
                </div>
                <Table rowKey="Id" loading={正在加载} bordered={true} rowSelection={{
                    onChange: (已选Id) => {
                        this.setState({ 已选Id })
                    },
                }} pagination={{
                    current: 页,
                    pageSize: 页长,
                    total: 总数
                }} onChange={分页 => {
                    this.setState({ 页: 分页.current, 页长: 分页.pageSize }, () => {
                        this.获取评论列表()
                    })
                }
                } columns={列} dataSource={评论列表 ? 评论列表 : []} />
            </div>
        )
    }
}

export default App
