import React, { Component } from 'react'
import {
    Icon, Button, Card, List, Input, Menu, Dropdown, message, Modal
} from 'antd'
import { connect } from 'dva'

import request from '../../../utils/request'
import utils from '../../../utils'
import 食谱信息窗口 from './食谱信息窗口'
import 添加模式窗口 from './添加模式窗口'
import 添加食谱窗口 from './添加食谱窗口'

@connect(({ 食谱, global }) => ({
    当前用户: global.当前用户,
    模式列表: 食谱.模式列表,
    食谱窗口显示: 食谱.食谱窗口显示,
    模式窗口显示: 食谱.模式窗口显示,
    食谱信息窗口显示: 食谱.食谱信息窗口显示
}))
class App extends Component {
    componentDidMount = () => {
        this.props.dispatch({ type: "食谱/获取模式列表" })
    }
    render() {
        const { dispatch, 模式列表, 食谱窗口显示, 模式窗口显示, 食谱信息窗口显示, 当前用户 } = this.props

        return (
            <div>
                <div style={{ marginBottom: 16, display: 'flex', alignItems: 'center' }}>
                    <Button type="primary" onClick={() => {
                        dispatch({ type: "食谱/save", 当前模式: {}, 模式窗口显示: true })
                    }}>添加模式</Button>

                    <Button style={{ marginLeft: 16, marginRight: 16 }} type="primary" onClick={() => {
                        dispatch({ type: "食谱/save", 当前食谱: {}, 食谱窗口显示: true })
                    }}>添加食谱</Button>

                    <Input.Search
                        placeholder="搜索名称或食材"
                        enterButton="搜索"
                        onSearch={(value) => {
                            dispatch({ type: "食谱/获取模式列表", 搜索: value })
                        }}
                        style={{ width: 256 }}
                    />

                    <div style={{ flex: 1 }}></div>

                    <Button type="primary">
                        <a href={"用户食谱/" + 当前用户.F昵称} target="_blank">分享</a>
                    </Button>
                </div>
                <List grid={{ gutter: 16, column: 4 }} dataSource={模式列表} renderItem={模式 => (
                    <List.Item>
                        <Card key={模式.Id} title={(<a>{模式.F名称}</a>)} extra={(<Dropdown overlay={(<Menu onClick={({ item, key }) => {
                            switch (key) {
                                case "编辑":
                                    dispatch({ type: "食谱/save", 模式窗口显示: true, 当前模式: 模式 })
                                    break;
                                case "删除":
                                    Modal.confirm({
                                        title: "确定要删除 " + 模式.F名称 + " 吗?",
                                        onOk: () => {
                                            request("模式/删除", {
                                                Id: 模式.Id
                                            }, () => {
                                                message.success("删除成功")
                                                dispatch({ type: "食谱/获取模式列表" })
                                            }, (code, msg) => {
                                                message.error(msg)
                                            })
                                        }
                                    })
                                    break;
                            }
                        }}>
                            <Menu.Item key="编辑">
                                编辑
                            </Menu.Item>
                            <Menu.Item key="删除">
                                删除
                            </Menu.Item >
                        </Menu>)}>
                            <Icon type="down" />
                        </Dropdown>)}>
                            <List size="large" dataSource={模式.F食谱列表 ? 模式.F食谱列表 : []} renderItem={食谱 => (
                                <List.Item key={食谱.Id}>
                                    <div style={{ display: 'flex', alignItems: 'center' }}>
                                        <div style={{ flex: 1 }} onClick={() => {
                                            dispatch({ type: "食谱/save", 食谱信息窗口显示: true, 当前食谱: 食谱 })
                                        }}>
                                            {食谱.F名称}
                                        </div>
                                        <Dropdown overlay={(<Menu onClick={({ item, key }) => {
                                            switch (key) {
                                                case "编辑":
                                                    dispatch({ type: "食谱/save", 食谱窗口显示: true, 当前食谱: 食谱 })
                                                    break;
                                                case "删除":
                                                    Modal.confirm({
                                                        title: "确定要删除 " + 食谱.F名称 + " 吗?",
                                                        onOk: () => {
                                                            request("食谱/删除", {
                                                                Id: 食谱.Id
                                                            }, () => {
                                                                message.success("删除成功")
                                                                dispatch({ type: "食谱/获取模式列表" })
                                                            }, (code, msg) => {
                                                                message.error(msg)
                                                            })
                                                        },
                                                    })
                                                    break;
                                            }
                                        }}>
                                            <Menu.Item key="编辑">
                                                编辑
                                        </Menu.Item>
                                            <Menu.Item key="删除">
                                                删除
                                        </Menu.Item>
                                        </Menu>)}>
                                            <Icon type="down" />
                                        </Dropdown>
                                    </div>
                                </List.Item>
                            )} />
                        </Card>
                    </List.Item>
                )}>
                </List>
                {食谱窗口显示 && <添加食谱窗口 />}
                {模式窗口显示 && <添加模式窗口 />}
                {食谱信息窗口显示 && <食谱信息窗口 />}
            </div>
        )
    }
}

export default App
