import React, { Component } from 'react'
import { Modal, List, Input, Button, message } from 'antd'
import { connect } from 'dva'

import request from '../../../utils/request'
import 食谱信息 from '../../单页/食谱信息/食谱信息'

@connect(({ 食谱 }) => ({ 当前食谱: 食谱.当前食谱, 食谱信息窗口显示: 食谱.食谱信息窗口显示 }))
class App extends Component {
    render() {
        const { dispatch, 当前食谱, 食谱信息窗口显示 } = this.props
        return (
            <Modal footer={null} visible={食谱信息窗口显示} title={当前食谱.F名称} onCancel={() => {
                dispatch({ type: "食谱/save", 当前食谱: {}, 食谱信息窗口显示: false })
            }}>
                <食谱信息 />
            </Modal>
        )
    }
}

export default App
