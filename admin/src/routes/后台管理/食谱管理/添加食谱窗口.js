import React, { Component } from 'react'
import { Modal, Form, Button, Input, message, Select } from 'antd'
import { connect } from 'dva'
const FormItem = Form.Item
const { Option } = Select
const { TextArea } = Input;

import request from '../../../utils/request'

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
    },
};

@connect(({ 食谱 }) => ({
    模式列表: 食谱.模式列表,
    当前食谱: 食谱.当前食谱,
    食谱窗口显示: 食谱.食谱窗口显示
}))
@Form.create()
class App extends Component {
    关闭食谱窗口 = () => {
        this.props.dispatch({ type: "食谱/save", 食谱窗口显示: false, 当前食谱: {} })
    }
    确定食谱窗口 = () => {
        const { 当前食谱, dispatch, form: { validateFields } } = this.props
        validateFields({ force: true }, (err, values) => {
            if (!err) {
                request("食谱/保存", {
                    ...当前食谱,
                    ...values
                }, (data) => {
                    localStorage.上次使用模式Id = values.F模式Id
                    dispatch({ type: "食谱/获取模式列表" })
                    this.关闭食谱窗口()
                }, (code, msg) => {
                    message.error(msg)
                    this.关闭食谱窗口()
                })
            }
        })
        this.setState({ 食谱窗口: false })
    }

    render() {
        const { 模式列表, 当前食谱, 食谱窗口显示, form: { getFieldDecorator, validateFields } } = this.props

        return (
            <Modal visible={食谱窗口显示} title={当前食谱.Id ? "修改食谱" : "添加食谱"} onCancel={this.关闭食谱窗口} onOk={this.确定食谱窗口}>
                <Form>
                    <FormItem {...formItemLayout} label="模式">
                        {getFieldDecorator("F模式Id", {
                            initialValue: 当前食谱.F模式Id ? 当前食谱.F模式Id : (localStorage.上次使用模式Id ? parseInt(localStorage.上次使用模式Id) : undefined),
                            rules: [{
                                required: true
                            }]
                        })(
                            <Select>
                                {模式列表.map((模式) => {
                                    return <Option key={模式.Id} value={模式.Id}>{模式.F名称}</Option>
                                })}
                            </Select>
                            )}
                    </FormItem>
                    <FormItem {...formItemLayout} label="名称">
                        {getFieldDecorator("F名称", {
                            initialValue: 当前食谱.F名称,
                            rules: [{
                                required: true
                            }]
                        })(
                            <Input />
                            )}
                    </FormItem>
                    <FormItem {...formItemLayout} label="食材">
                        {getFieldDecorator("F食材", {
                            initialValue: 当前食谱.F食材,
                            rules: [{
                                required: true
                            }]
                        })(
                            <TextArea autosize={true} />
                            )}
                    </FormItem>
                    <FormItem {...formItemLayout} label="做法">
                        {getFieldDecorator("F做法", {
                            initialValue: 当前食谱.F做法,
                            rules: [{
                                required: true
                            }]
                        })(
                            <TextArea autosize={true} />
                            )}
                    </FormItem>
                </Form>
            </Modal>
        )
    }
}

export default App
