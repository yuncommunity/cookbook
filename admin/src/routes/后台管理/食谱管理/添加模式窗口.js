import React, { Component } from 'react'
import { Modal, Form, Button, Input, Popconfirm, message } from 'antd'
import { connect } from 'dva'
const FormItem = Form.Item

import request from '../../../utils/request'

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
    },
};

@connect(({ 食谱 }) => ({
    当前模式: 食谱.当前模式,
    模式窗口显示: 食谱.模式窗口显示
}))
@Form.create()
class App extends Component {
    关闭模式窗口 = () => {
        this.props.dispatch({ type: "食谱/save", 模式窗口显示: false, 当前模式: {} })
    }
    确定模式窗口 = () => {
        const { 当前模式, dispatch, form: { validateFields } } = this.props
        validateFields({ force: true }, (err, values) => {
            if (!err) {
                request("模式/保存", {
                    ...当前模式,
                    ...values
                }, (data) => {
                    dispatch({ type: "食谱/获取模式列表" })
                    this.关闭模式窗口()
                }, (code, msg) => {
                    message.error(msg)
                    this.关闭模式窗口()
                })
            }
        })
    }

    render() {
        const { 当前模式, 模式窗口显示, form: { getFieldDecorator, validateFields } } = this.props

        return (
            <Modal visible={模式窗口显示} title={当前模式.Id ? "修改模式" : "新增模式"} onCancel={this.关闭模式窗口} onOk={this.确定模式窗口}>
                <Form>
                    <FormItem {...formItemLayout} label="模式名称">
                        {getFieldDecorator("F名称", {
                            initialValue: 当前模式.F名称,
                            rules: [{
                                required: true
                            }]
                        })(
                            <Input size="large" placeholder="模式名称" />
                            )}
                    </FormItem>
                </Form>
            </Modal>
        )
    }
}

export default App
