import React, { Component } from 'react'
import { Table, Button, Input, Popconfirm, message } from 'antd'
import request from '../../../utils/request'
import utils from '../../../utils'

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            页: 1,
            页长: 10,
            总数: 0,
            用户列表: [],
            已选Id: [],
            正在加载: false,
            搜索: ""
        }
    }
    componentDidMount = () => {
        this.获取用户列表()
    }
    获取用户列表 = () => {
        this.setState({ 正在加载: true })

        const { 页, 页长, 搜索 } = this.state
        request("用户/管理列表", {
            页, 页长, 搜索
        }, (用户列表, 总数) => {
            this.setState({ 用户列表, 总数, 正在加载: false })
        })
    }

    render() {
        const { 页, 页长, 总数, 用户列表, 选择, 正在加载, 已选Id } = this.state
        const 列 = [{
            title: "ID",
            dataIndex: "Id",
        }, {
            title: "名称",
            dataIndex: "F昵称",
        }, {
            title: "联系方式",
            dataIndex: "F邮箱",
        }, {
            title: "创建时间",
            dataIndex: "F创建时间",
            render: (text, record) => {
                return utils.格式化时间(text)
            }
        }]
        return (
            <div>
                <div style={{ marginBottom: 16, display: 'flex', justifyContent: 'space-between' }}>
                    <Input.Search style={{ width: 256 }} enterButton placeholder="输入用户名称" onSearch={搜索 => {
                        this.setState({ 搜索, 页: 1 }, () => {
                            this.获取用户列表()
                        })
                    }} />

                    {已选Id.length > 0 && (<Popconfirm title="确定要删除吗?" onConfirm={() => {
                        request("用户/删除", {
                            Ids: JSON.stringify(已选Id)
                        }, () => {
                            this.获取用户列表()
                        }, (code, msg) => {
                            message(msg)
                        })
                    }}>
                        <Button type="danger">删除</Button>
                    </Popconfirm>)}
                </div>
                <Table rowKey="Id" loading={正在加载} bordered={true} rowSelection={{
                    onChange: (已选Id) => {
                        this.setState({ 已选Id })
                    },
                }} pagination={{
                    current: 页,
                    pageSize: 页长,
                    total: 总数
                }} onChange={(分页, 筛选, 排序) => {
                    this.setState({ 页: 分页.current, 页长: 分页.pageSize }, () => {
                        this.获取用户列表()
                    })
                }
                } columns={列} dataSource={用户列表 ? 用户列表 : []} />
            </div>
        )
    }
}

export default App
