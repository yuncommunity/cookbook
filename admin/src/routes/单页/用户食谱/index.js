import React, { Component } from 'react'
import {
    Icon, Button, Card, List, Input, Menu, Dropdown, message, Modal, Layout
} from 'antd'
import { Link } from 'dva/router'
import { connect } from 'dva'

import request from '../../../utils/request'
import utils from '../../../utils'
import 食谱信息窗口 from '../../后台管理/食谱管理/食谱信息窗口'
import styles from './index.less'

const { Header, Content } = Layout;

@connect(({ 食谱 }) => ({
    模式列表: 食谱.模式列表,
    食谱信息窗口显示: 食谱.食谱信息窗口显示
}))
class App extends Component {
    componentDidMount = () => {
        this.获取模式列表()
    }
    获取模式列表 = (搜索) => {
        let 用户名称 = this.props.match.params.name
        this.props.dispatch({ type: "食谱/获取模式列表", 用户名称, 搜索 })
    }
    render() {
        const { dispatch, 模式列表, 食谱信息窗口显示 } = this.props

        return (
            <Layout style={{ height: '100%' }}>
                <Header className={styles.header}>
                    <div className={styles.logo}>
                        <Link to="/">
                            <img src="https://gw.alipayobjects.com/zos/rmsportal/iwWyPinUoseUxIAeElSx.svg" alt="logo" />
                            <h1>食谱</h1>
                        </Link>

                        <Input.Search
                            placeholder="搜索名称或食材"
                            enterButton="搜索"
                            onSearch={(value) => {
                                this.获取模式列表(value)
                            }}
                            style={{ width: 256, marginLeft: 100 }}
                        />
                    </div>
                </Header>
                <List style={{ padding: 16 }} grid={{ gutter: 16, column: 4 }} dataSource={模式列表} renderItem={模式 => (
                    <List.Item>
                        <Card key={模式.Id} title={(<a>{模式.F名称}</a>)}>
                            <List size="large" dataSource={模式.F食谱列表 ? 模式.F食谱列表 : []} renderItem={食谱 => (
                                <List.Item key={食谱.Id}>
                                    <List.Item.Meta title={食谱.F名称} style={{ width: '100%' }} onClick={() => {
                                        dispatch({ type: "食谱/save", 食谱信息窗口显示: true, 当前食谱: 食谱 })
                                    }}></List.Item.Meta>
                                </List.Item>
                            )} />
                        </Card>
                    </List.Item>
                )}>
                </List>
                {食谱信息窗口显示 && <食谱信息窗口 />}
            </Layout>
        )
    }
}

export default App
