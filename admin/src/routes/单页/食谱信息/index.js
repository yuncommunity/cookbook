import React, { Component } from 'react'
import { Modal, List, Input, Button, message, Layout, } from 'antd'
import { Link } from 'dva/router'
import { connect } from 'dva'

import request from '../../../utils/request'
import styles from './index.less'
import 食谱信息 from './食谱信息'

const { Header, Content } = Layout;

@connect(({ 食谱 }) => ({ 当前食谱: 食谱.当前食谱, 食谱信息窗口显示: 食谱.食谱信息窗口显示 }))
class App extends Component {
    componentDidMount = () => {
        let id = this.props.match.params.id
        if (id) {
            request("食谱/详情", { id }, 当前食谱 => {
                this.props.dispatch({ type: "食谱/save", 当前食谱 })
            })
        }
    }
    render() {
        const { 当前食谱 } = this.props
        return (
            <Layout style={{ height: '100%' }}>
                <Header className={styles.header}>
                    <div className={styles.logo}>
                        <Link to="/">
                            <img src="https://gw.alipayobjects.com/zos/rmsportal/iwWyPinUoseUxIAeElSx.svg" alt="logo" />
                            <h1>食谱</h1>
                        </Link>

                        <h1 style={{ marginLeft: 16 }}>{"- " + 当前食谱.F名称}</h1>
                    </div>
                </Header>
                <div style={{ display: 'flex', justifyContent: 'center', paddingTop: 16 }} onClick={(e) => {
                    this.setState({ 显示评论: false })
                }}>
                    <div style={{ width: 480 }}>
                        {当前食谱.Id && <食谱信息 />}
                    </div>
                </div>
            </Layout>
        )
    }
}

export default App
