import React, { Component } from 'react'
import { Modal, List, Input, Form, Button, message, Layout, } from 'antd'
import { Link } from 'dva/router'
import { connect } from 'dva'
import moment from 'moment'
const FormItem = Form.Item

import request from '../../../utils/request'
import styles from './index.less'

const { Header, Content } = Layout;

@Form.create()
@connect(({ 食谱 }) => ({ 当前食谱: 食谱.当前食谱, 食谱信息窗口显示: 食谱.食谱信息窗口显示 }))
class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            评论列表: [],
            显示评论: false
        }
    }
    componentDidMount = () => {
        this.获取评论列表()
    }
    获取评论列表 = () => {
        const { 当前食谱 } = this.props
        request("评论/列表", { F食谱Id: 当前食谱.Id }, 评论列表 => {
            this.setState({ 评论列表 })
        })
    }

    确认评论 = () => {
        const { dispatch, 当前食谱, form: { validateFields, resetFields } } = this.props
        validateFields({ force: true }, (err, values) => {
            if (!err) {
                request("评论/提交", {
                    ...values,
                    F食谱Id: 当前食谱.Id
                }, () => {
                    localStorage.F昵称 = values.F昵称
                    localStorage.F联系方式 = values.F联系方式
                    resetFields()
                    this.获取评论列表()
                }, (code, msg) => {
                    message.error(msg)
                })
            }
        })
    }
    render() {
        const { 评论列表, 显示评论 } = this.state
        const { dispatch, 当前食谱, 食谱信息窗口显示, form: { getFieldDecorator } } = this.props
        return (
            <div style={{ width: '100%' }} onClick={(e) => {
                this.setState({ 显示评论: false })
            }}>
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>食材: <a href={"/食谱信息/" + 当前食谱.Id} target="_blank">分享</a></div>
                <div style={{
                    padding: 10,
                    marginBottom: 10,
                    color: 'black',
                    borderBottom: '1px solid #d9d9d9'
                }}
                    dangerouslySetInnerHTML={{ __html: 当前食谱.F食材 ? 当前食谱.F食材.replaceAll("\n", "<br />", -1) : "" }}></div>
                <div>做法:</div>
                <div style={{ padding: 10, marginBottom: 10, color: 'black', borderBottom: '1px solid #d9d9d9' }}
                    dangerouslySetInnerHTML={{ __html: 当前食谱.F做法 ? 当前食谱.F做法.replaceAll("\n", "<br />", -1) : "" }}></div>

                <Form onClick={(e) => {
                    e.stopPropagation()
                }}>
                    {显示评论 && (
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <FormItem style={{ height: 10, flex: 1 }}>
                                {getFieldDecorator("F昵称", {
                                    initialValue: localStorage.F昵称 ? localStorage.F昵称 : "",
                                    rules: [{
                                        required: false
                                    }]
                                })(
                                    <Input placeholder="昵称" />
                                    )}
                            </FormItem>
                            <div style={{ width: 80, paddingLeft: 10 }}>
                                昵称
                                </div>
                        </div>
                    )}
                    {显示评论 && (
                        <div style={{ display: 'flex', alignItems: 'center', marginTop: 6, marginBottom: 6 }}>
                            <FormItem style={{ height: 10, flex: 1 }}>
                                {getFieldDecorator("F联系方式", {
                                    initialValue: localStorage.F联系方式 ? localStorage.F联系方式 : "",
                                    rules: [{
                                        required: false
                                    }]
                                })(
                                    <Input placeholder="联系方式(邮箱/手机号/QQ...)" />
                                    )}
                            </FormItem>
                            <div style={{ width: 80, paddingLeft: 10 }}>
                                联系方式
                                </div>
                        </div>
                    )}
                    <FormItem style={{ height: 10 }}>
                        {getFieldDecorator("F评论内容", {
                            rules: [{
                                required: true,
                                message: "请输入评论内容"
                            }]
                        })(
                            <Input.Search placeholder="评论" onFocus={e => {
                                this.setState({ 显示评论: true })
                            }} onSearch={this.确认评论} enterButton="确认" />
                            )}
                    </FormItem>
                </Form>

                <List dataSource={评论列表 ? 评论列表 : []} renderItem={评论 => (
                    <List.Item>
                        <List.Item.Meta title={评论.F昵称} description={评论.F评论内容}></List.Item.Meta>
                        {moment(评论.F创建时间).fromNow()}
                    </List.Item>
                )}></List>
            </div>
        )
    }
}

export default App
