import React, { Component } from 'react'
import { Form, Icon, Input, Button, Alert } from 'antd';
const FormItem = Form.Item;
import { connect } from 'dva'
import { routerRedux, Link } from 'dva/router';

import request from '../../../utils/request'

function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}

@connect()
@Form.create()
class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            错误信息: ""
        }
    }
    componentDidMount() {
        this.props.form.validateFields();
    }
    登录 = () => {
        const { dispatch, form } = this.props
        form.validateFields((err, values) => {
            if (!err) {
                request("用户/登录", values, (用户) => {
                    localStorage.Token = 用户.Token
                    dispatch(routerRedux.push("/"))
                }, (code, msg) => {
                    this.setState({ 错误信息: msg })
                })
            }
        });
    }
    render() {
        const { 错误信息 } = this.state
        const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;

        const 用户名Error = isFieldTouched('用户名') && getFieldError('用户名');
        const 密码Error = isFieldTouched('密码') && getFieldError('密码');
        return (
            <Form style={{
                width: 320,
                margin: '0 auto'
            }}>
                {错误信息 && (
                    <Alert type="error" showIcon message={错误信息} style={{ marginBottom: 24 }}></Alert>
                )}
                <FormItem
                    validateStatus={用户名Error ? 'error' : ''}
                    help={用户名Error || ''}
                >
                    {getFieldDecorator('用户名', {
                        initialValue: "admin",
                        rules: [{ required: true, message: '请输入用户名' }],
                    })(
                        <Input size="large" prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="用户名" />
                        )}
                </FormItem>
                <FormItem
                    validateStatus={密码Error ? 'error' : ''}
                    help={密码Error || ''}
                >
                    {getFieldDecorator('密码', {
                        initialValue: "admin",
                        rules: [{ required: true, message: '请输入密码' }],
                    })(
                        <Input size="large" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="密码" />
                        )}
                </FormItem>
                <FormItem>
                    <Button
                        style={{ width: "100%" }}
                        size="large"
                        type="primary"
                        onClick={this.登录}
                        disabled={hasErrors(getFieldsError())}
                    >
                        登录
                    </Button>
                </FormItem>

                <div style={{ textAlign: 'right' }}>
                    <Link to="/用户/注册">注册账户</Link>
                </div>
            </Form>
        );
    }
}

export default App
