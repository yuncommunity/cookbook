import '@babel/polyfill';
import dva from 'dva';

// import createHistory from 'history/createHashHistory';
// user BrowserHistory
import createHistory from 'history/createBrowserHistory';
import createLoading from 'dva-loading';
import 'moment/locale/zh-cn';
import FastClick from 'fastclick';
import './rollbar';
import models from './models'

import './index.less';
// 初始化 dva,使用 browserHistory
const app = dva({
  history: createHistory(),
});

// 加载 loading 插件
app.use(createLoading());

// 加载所有model
models.forEach(model => {
  app.model(model.default)
});

// 加载路由配置
app.router(require('./router').default);

// 启动 dva
app.start('#root');

FastClick.attach(document.body);

export default app._store;  // eslint-disable-line
