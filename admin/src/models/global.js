
import request from '../utils/request'
import { routerRedux } from 'dva/router';
export default {
    namespace: 'global',
    state: {
        collapsed: false,
        当前用户: {}
    },
    effects: {
        *获取用户信息({ }, { put }) {
            const 请求结果 = yield request("用户/获取用户信息")
            if (请求结果 && 请求结果.返回码 == 0) {
                yield put({ type: 'save', 当前用户: 请求结果.数据 })
            }
        },
        *退出登录({ }, { put }) {
            localStorage.Token = ""
            yield put({ type: 'save', 当前用户: {} })
            yield put(routerRedux.push("/用户/登录"))
        }
    },

    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action
            }
        },
        changeLayoutCollapsed(state, { payload }) {
            return {
                ...state,
                collapsed: payload,
            };
        },
    },

    subscriptions: {
        setup({ history }) {
            // Subscribe history(url) change, trigger `load` action if pathname is `/`
            return history.listen(({ pathname, search }) => {
                if (typeof window.ga !== 'undefined') {
                    window.ga('send', 'pageview', pathname + search);
                }
            });
        },
    },
};
