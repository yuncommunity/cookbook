import request from '../utils/request'

export default {
    namespace: "食谱",
    state: {
        模式列表: [],
        食谱窗口显示: false,
        模式窗口显示: false,
        食谱信息窗口显示: false,
        当前模式: {},
        当前食谱: {}
    },
    effects: {
        *获取模式列表({ 搜索, 用户名称 }, { put }) {
            const 请求结果 = yield request("模式/食谱列表", { 搜索, 用户名称 })
            if (请求结果.返回码 == 0 && 请求结果.数据) {
                yield put({ type: "save", 模式列表: 请求结果.数据 })
            } else {
                yield put({ type: "save", 模式列表: [] })
            }
        }
    },
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action
            };
        },
    },
};

