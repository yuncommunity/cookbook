# babel 及 Ant Design Pro 升级导致无法运行,已停止维护

# 食谱

使用 golang + react 开发的简易小项目

 - 在线示例 http://yuncommunity.com:8040/
 - 源码地址 https://gitee.com/yuncommunity/cookbook
 - 视频教程 http://study.163.com/course/courseMain.htm?courseId=1004935038&utm_campaign=commission&utm_source=cp-400000000375010&utm_medium=share

## 目录简介

 - admin 前端界面,使用 Ant Design Pro(1.0.1) 模板
 - api 后台接口,使用 beego 框架
 - pro1.0.1 前端使用的模板源码,即 Ant Design Pro, 版本: 1.0.1
 - build.sh 打包 admin js,放入 api 中.
 - push.sh commit 并且 push 代码到 git 服务器
 - restart.sh 重启api

## 使用框架

- 后台主框架 beego https://beego.me/
- 数据库ORM storm https://github.com/asdine/storm
- 前端模板 Ant Design Pro https://pro.ant.design/docs/getting-started-cn
    - 处理路由和全局 state 使用 dva https://github.com/dvajs/dva
    - build js 和 热加载使用 roadhog https://github.com/sorrycc/roadhog

## 下载并运行

需要先配置 golang 和 npm 开发环境

- 下载代码

````
 cd $GOPATH/src
 git clone https://gitee.com/yuncommunity/cookbook.git
 cd cookbook
 git checkout tags/v1.0
````

- 运行后台接口

需要配置 $GOPATH/bin 到 $PATH 中,例如:添加下列代码到 ~/.profile 底部

````
export PATH=$PATH:$GOPATH/bin
````

然后使用 bee 运行 [bee 工具简介](https://beego.me/docs/install/bee.md)

 ````
 cd $GOPATH/src/cookbook/api
 go get
 go get -u github.com/beego/bee
 bee run
 ````

 打开浏览器 http://localhost:8040/ 可以查看是否启动成功

 - 运行前端界面

 ````
 cd $GOPATH/src/cookbook/admin
 npm i
 npm start
 ````

 打开浏览器 http://localhost:8041/ 可以查看是否启动成功(一般需要等几十秒)