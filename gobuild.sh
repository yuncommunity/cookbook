cd 添加版本号
go run main.go
cd ../api

# 编译linux版本
env GOOS=linux GOARCH=amd64 go build main.go
mv -f main cookbook_linux
# 编译windows 32位版本
env GOOS=windows GOARCH=386 go build main.go
mv -f main.exe cookbook_win32.exe
# 编译windows 64位版本
env GOOS=windows GOARCH=amd64 go build main.go
mv -f main.exe cookbook_win64.exe
# 编译mac版本
env GOOS=darwin GOARCH=amd64 go build main.go
mv -f main cookbook_mac

tar zcvf ../cookbook.tar.gz cookbook_* conf static views

cd ..
# 同步代码到版本控制(git)
./push.sh

# yuncommunity.com 为部署的域名,没有部署则不用这一步
curl http://yuncommunity.com:8040/更新