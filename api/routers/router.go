package routers

import (
	"cookbook/api/controllers"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/plugins/cors"
)

func init() {
	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "Authorization", "Access-Control-Allow-Origin", "X-Requested-With"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
	}))

	ns := beego.NewNamespace("/接口",
		beego.NSNamespace("/用户", beego.NSInclude(&controllers.C用户{})),
		beego.NSNamespace("/模式", beego.NSInclude(&controllers.C模式{})),
		beego.NSNamespace("/食谱", beego.NSInclude(&controllers.C食谱{})),
		beego.NSNamespace("/评论", beego.NSInclude(&controllers.C评论{})),
	)

	beego.AddNamespace(ns)

	beego.Router("/更新", &controllers.MainController{}, "*:A更新")
	beego.Router("/*", &controllers.MainController{})
}
