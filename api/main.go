package main

import (
	"cookbook/api/controllers"
	"cookbook/api/models"
	_ "cookbook/api/routers"
	"os"
	"time"

	"github.com/asdine/storm/q"

	"github.com/asdine/storm"
	"github.com/astaxie/beego"
	"github.com/oldfeel/ofutils"
)

func main() {
	isFirst := false
	if !ofutils.Exist("db") {
		os.MkdirAll("db", os.ModePerm)
	}
	if !ofutils.Exist("db/data.db") {
		isFirst = true
	}

	db, err := storm.Open("db/data.db")
	if err != nil {
		beego.Error(err)
	}
	count, _ := db.Select(q.Eq("F昵称", "admin")).Count(&models.M用户{})
	if isFirst || (count == 0) {
		db.Save(&models.M用户{F昵称: "admin", F密码: ofutils.MD5("admin"), F创建时间: time.Now()})
	}
	controllers.DB = db

	beego.Run()
}
