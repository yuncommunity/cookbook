package controllers

import (
	"cookbook/api/models"
	"encoding/json"
	"time"

	"github.com/asdine/storm/q"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/cache"
	"github.com/astaxie/beego/utils/captcha"
	"github.com/oldfeel/ofutils"
)

var cpt *captcha.Captcha

func init() {
	store := cache.NewMemoryCache()
	cpt = captcha.NewWithFilter("/验证码图片/", store)
	cpt.StdWidth = 118
	cpt.StdHeight = 40
}

type C用户 struct {
	BaseController
}

// @router /登录 [get]
func (c *C用户) A登录() {
	用户名 := c.GetString("用户名")
	F密码 := c.GetString("密码")

	var 用户 models.M用户

	err := DB.Select(q.Eq("F昵称", 用户名), q.Eq("F密码", ofutils.MD5(F密码))).First(&用户)
	if err != nil {
		c.Fail(1, "帐号或密码错误")
		return
	}

	用户.Token = ofutils.RandString(32)
	DB.Save(&用户)

	c.Success(用户.G信息())
}

// @router /注册 [get]
func (c *C用户) A注册() {
	用户名 := c.GetString("用户名")
	密码 := c.GetString("密码")
	邮箱 := c.GetString("邮箱")
	验证码Id := c.GetString("验证码Id")
	验证码 := c.GetString("验证码")
	if !cpt.Verify(验证码Id, 验证码) {
		c.Fail(1, "验证码错误")
		return
	}
	用户 := models.M用户{F昵称: 用户名, F密码: ofutils.MD5(密码), F邮箱: 邮箱, F创建时间: time.Now()}
	err := DB.Save(&用户)
	if err != nil {
		c.Fail(1, err)
		return
	}
	c.Success(用户)
}

// @router /检测用户名 [get]
func (c *C用户) A检测用户名() {
	用户名 := c.GetString("用户名")
	u := c.GetUser()

	var m models.M用户
	err := DB.Select(q.Eq("F昵称", 用户名)).First(&m)
	if err != nil {
		beego.Info("检测用户名", err)
	}
	c.Success(m.Id == 0 || m.Id == u.Id)
}

// @router /验证码 [get]
func (c *C用户) A验证码() {
	id, err := cpt.CreateCaptcha()
	if err != nil {
		c.Fail(1, err)
		return
	}
	c.Success(id)
}

// @router /获取用户信息 [get]
func (c *C用户) A获取用户信息() {
	if c.UnLogin() {
		return
	}
	用户 := c.GetUser()

	c.Success(用户.G信息())
}

// @router /管理列表 [get]
func (c *C用户) A管理列表() {
	页, _ := c.GetInt("页")
	页长, _ := c.GetInt("页长")
	搜索 := c.GetString("搜索")
	页 = 页 - 1

	var 列表 []models.M用户
	query := DB.Select(q.Re("F昵称", 搜索)).Limit(页长).Skip(页长 * 页).Reverse()
	err := query.Find(&列表)
	if err != nil {
		beego.Info(err)
	}
	for i, _ := range 列表 {
		列表[i].F密码 = ""
	}
	c.Page(列表, &models.M用户{}, query)
}

// @router /删除 [get]
func (c *C用户) A删除() {
	Ids := c.GetString("Ids")
	var ids []int
	err := json.Unmarshal([]byte(Ids), &ids)
	if err != nil {
		c.Fail(1, err)
		return
	}

	if (len(ids) == 1) && (ids[0] == 6) {
		c.Fail(2, "admin 不能删除")
		return
	}
	for _, v := range ids {
		if v == 6 {
			continue
		}
		err := DB.Delete("M用户", v)
		if err != nil {
			beego.Info(v, err)
		}
	}
	c.Success("删除成功")
}
