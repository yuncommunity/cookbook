package controllers

import (
	"cookbook/api/models"
	"time"

	"github.com/asdine/storm/q"
)

type C食谱 struct {
	BaseController
}

// @router /保存 [get]
func (c *C食谱) A保存() {
	if c.UnLogin() {
		return
	}

	var m models.M食谱
	err := c.ParseForm(&m)
	if err != nil {
		c.Fail(1, err)
		return
	}

	var tmp models.M食谱
	err = DB.Select(q.Eq("F名称", m.F名称)).First(&tmp)
	if err == nil && tmp.Id != m.Id {
		c.Fail(2, tmp.F名称+"已经存在")
		return
	}

	if tmp.Id != 0 {
		m.F创建时间 = tmp.F创建时间
	} else {
		m.F创建时间 = time.Now()
	}

	err = DB.Save(&m)
	if err != nil {
		c.Fail(3, err)
		return
	}
	c.Success(m)
}

// @router /删除 [get]
func (c *C食谱) A删除() {
	if c.UnLogin() {
		return
	}

	id, _ := c.GetInt("Id")
	err := DB.DeleteStruct(&models.M食谱{Id: id})
	if err != nil {
		c.Fail(1, err)
		return
	}
	c.Success("删除成功")
}

// @router /详情 [get]
func (c *C食谱) A详情() {
	id, _ := c.GetInt("id")
	var 食谱 models.M食谱
	err := DB.One("Id", id, &食谱)
	if err != nil {
		c.Fail(1, err)
		return
	}
	c.Success(食谱)
}
