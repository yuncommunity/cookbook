package controllers

import (
	"cookbook/api/models"
	"encoding/json"
	"time"

	"github.com/asdine/storm/q"
	"github.com/astaxie/beego"
)

type C评论 struct {
	BaseController
}

// @router /提交 [get]
func (c *C评论) A提交() {
	var 评论 models.M评论
	err := c.ParseForm(&评论)
	if err != nil {
		c.Fail(1, err)
		return
	}
	评论.F创建时间 = time.Now()
	err = DB.Save(&评论)
	if err != nil {
		c.Fail(2, err)
		return
	}
	c.Success(评论)
}

// @router /列表 [get]
func (c *C评论) A列表() {
	F食谱Id, _ := c.GetInt("F食谱Id")
	var 列表 []models.M评论
	err := DB.Select(q.Eq("F食谱Id", F食谱Id)).OrderBy("F创建时间").Reverse().Find(&列表)
	if err != nil {
		beego.Info(err)
	}
	c.Success(列表)
}

// @router /管理列表 [get]
func (c *C评论) A管理列表() {
	页, _ := c.GetInt("页")
	页长, _ := c.GetInt("页长")
	搜索 := c.GetString("搜索")
	页 = 页 - 1

	var 列表 []models.M评论
	query := DB.Select(q.Re("F昵称", 搜索)).Limit(页长).Skip(页长 * 页).Reverse()
	err := query.Find(&列表)
	if err != nil {
		beego.Info(err)
	}
	for i, v := range 列表 {
		err := DB.One("Id", v.F食谱Id, &列表[i].F食谱)
		if err != nil {
			beego.Info(err)
		}
	}
	c.Page(列表, &models.M评论{}, query)
}

// @router /删除 [get]
func (c *C评论) A删除() {
	Ids := c.GetString("Ids")
	var ids []int
	err := json.Unmarshal([]byte(Ids), &ids)
	if err != nil {
		c.Fail(1, err)
		return
	}

	for _, v := range ids {
		err := DB.Delete("M评论", v)
		if err != nil {
			beego.Info(v, err)
		}
	}
	c.Success("删除成功")
}
