package controllers

import (
	"cookbook/api/models"
	"time"

	"github.com/asdine/storm/q"
	"github.com/astaxie/beego"
)

type C模式 struct {
	BaseController
}

// @router /保存 [get]
func (c *C模式) A保存() {
	if c.UnLogin() {
		return
	}

	var m models.M模式
	err := c.ParseForm(&m)
	if err != nil {
		c.Fail(1, err)
		return
	}

	var tmp models.M模式
	err = DB.Select(q.Eq("F名称", m.F名称)).First(&tmp)
	if err == nil && tmp.Id != m.Id {
		c.Fail(2, tmp.F名称+"已经存在")
		return
	}

	if tmp.Id != 0 {
		m.F创建时间 = tmp.F创建时间
	} else {
		m.F创建时间 = time.Now()
	}

	用户 := c.GetUser()
	m.F用户Id = 用户.Id
	err = DB.Save(&m)
	if err != nil {
		c.Fail(3, err)
		return
	}
	c.Success(m)
}

// @router /列表 [get]
func (c *C模式) A列表() {
	var 列表 []models.M模式
	err := DB.All(&列表)
	if err != nil {
		beego.Error(err)
	}
	c.Success(列表)
}

// @router /删除 [get]
func (c *C模式) A删除() {
	if c.UnLogin() {
		return
	}

	id, _ := c.GetInt("Id")
	err := DB.DeleteStruct(&models.M模式{Id: id})
	if err != nil {
		c.Fail(1, err)
		return
	}
	c.Success("删除成功")
}

// @router /食谱列表 [get]
func (c *C模式) A食谱列表() {
	用户 := c.GetUser()
	用户名称 := c.GetString("用户名称")
	搜索 := c.GetString("搜索")

	if 用户名称 != "" { // 查看指定用户的食谱
		err := DB.Select(q.Eq("F昵称", 用户名称)).First(&用户)
		if err != nil {
			c.Fail(1, err)
			return
		}
	}

	if 用户.Id == 0 {
		c.Fail(2, "未知用户")
		return
	}

	var 列表 []models.M模式
	err := DB.Select(q.Eq("F用户Id", 用户.Id)).Find(&列表)
	if err != nil {
		beego.Error(&列表)
	} else {
		for i, v := range 列表 {
			DB.Select(q.Eq("F模式Id", v.Id), q.Or(q.Re("F名称", 搜索), q.Re("F食材", 搜索))).Find(&列表[i].F食谱列表)
		}
	}
	c.Success(&列表)
}
