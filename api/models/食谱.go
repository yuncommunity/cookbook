package models

import (
	"time"
)

type M食谱 struct {
	Id    int `storm:"id,increment"`
	F模式Id int
	F名称   string
	F食材   string
	F做法   string
	F创建时间 time.Time
}
