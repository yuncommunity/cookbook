package models

import (
	"time"
)

type M评论 struct {
	Id    int `storm:"id,increment"`
	F食谱Id int
	F昵称   string
	F联系方式 string
	F评论内容 string
	F创建时间 time.Time
	F食谱   M食谱
}
